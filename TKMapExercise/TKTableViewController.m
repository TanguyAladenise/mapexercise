//
//  TKTableViewController.m
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 21/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

#import "TKTableViewController.h"
#import "TKAddressTableViewCell.h"
#import "TKMapViewController.h"
#import "TKEntityManager.h"
#import "Address.h"

@interface TKTableViewController ()

 /**
 *  addresses
 *
 *      Array of Address object loaded from core data
 */
@property (nonatomic) NSMutableArray *addresses;

@end

@implementation TKTableViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Edit button for deletion
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Fetch addresses object in core data
    self.addresses = [[TKEntityManager fetchAllForEntity:@"Address"] mutableCopy];
}



#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.addresses.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TKAddressTableViewCell *cell = (TKAddressTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"AddressCellIdentifier" forIndexPath:indexPath];
    
    // Configure the cell
    Address *address        = self.addresses[indexPath.row];
    cell.addressLabel.text  = [NSString stringWithFormat:@"%@, %@", address.streetNumber, address.street];
    cell.addressLabel2.text = [NSString stringWithFormat:@"%@, %@, %@", address.zipcode, address.province, address.city];
    
    return cell;
}


// Deletion.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Remove from core data
        [TKEntityManager removeObject:self.addresses[indexPath.row]];
        // Remove from data
        [self.addresses removeObjectAtIndex:indexPath.row];
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}


#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"TableToMapSegueIdentifier"]) {
        // Get the address selected by the user and display it on the map
        NSInteger i = [self.tableView indexPathForCell:sender].row;
        TKMapViewController *mapViewController = segue.destinationViewController;
        mapViewController.addresses = @[self.addresses[i]];
    }
}


#pragma mark - Setters


// Only reload tableView when necessary
- (void)setAddresses:(NSMutableArray *)addresses
{
    _addresses = addresses;
    
    [self.tableView reloadData];
}

@end
