//
//  TKPointAddressAnnotation.h
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 22/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

#import <MapKit/MapKit.h>

@class Address;

@interface TKPointAddressAnnotation : MKPointAnnotation

@property (nonatomic) Address *annotationAddress;

@end
