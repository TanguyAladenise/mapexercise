//
//  TKMapViewController.m
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 21/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

// Apple framework
#import <MapKit/MapKit.h>


#import "TKMapViewController.h"
#import "TKPointAddressAnnotation.h"
#import "TKShareView.h"
#import "TKEntityManager.h"
#import "Address.h"

@interface TKMapViewController ()

/**
 *  mapView
 *
 *      The mapView of the controller
 */
@property IBOutlet MKMapView *mapView;

@end


@implementation TKMapViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // If no addresses passed with navigation, fetch them
    if (self.addresses.count == 0) {
        self.addresses = [TKEntityManager fetchAllForEntity:@"Address"];
    }
    
    // For each address create map annotations
    for (Address *address in self.addresses) {
        [self addAnnotationForAddress:address];
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Center map on annotations
    [self.mapView showAnnotations:self.mapView.annotations animated:YES];
}

#pragma mark - UI actions

- (void)sharePosition:(MKPinAnnotationView *)sender
{
    // No multiple selection so first object it the one to be shared
    if (![self.view viewWithTag:1]) {
        [self setupShareview];
    }
}

// Present email
- (void)shareEmailButtonPressed
{
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate          = self;
        NSString *body                                  = [self addressBodyMessageToShare];
        [mailViewController setMessageBody:body isHTML:NO];
        
        [self presentViewController:mailViewController animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"alert.mail.title", nil)
                              message:NSLocalizedString(@"alert.mail.message", nil)
                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"alert.dismiss", nil)
                              otherButtonTitles:nil];
        [alert show];
    }
}

// Present message
- (void)shareMessageButtonPressed
{
    NSString *body = [self addressBodyMessageToShare];

    if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate          = self;
        messageController.body                            = body;
        
        [self presentViewController:messageController animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"alert.message.title", nil)
                              message:NSLocalizedString(@"alert.message.message", nil)
                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"alert.dismiss", nil)
                              otherButtonTitles:nil];
        [alert show];
    }
}


#pragma mark - Map view delegate


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKPinAnnotationView *mapPin = nil;

    static NSString *defaultPinID = @"defaultPin";
    mapPin = (MKPinAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if (!mapPin)
    {
        mapPin                = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        mapPin.canShowCallout = YES;
        UIButton *shareButton  = [UIButton buttonWithType:UIButtonTypeCustom];
        [shareButton setImage:[UIImage imageNamed:@"ico-share"] forState:UIControlStateNormal];
        shareButton.frame = CGRectMake(0, 0, 45, 45);
        [shareButton addTarget:self action:@selector(sharePosition:) forControlEvents:UIControlEventTouchUpInside];
        mapPin.rightCalloutAccessoryView = shareButton;
    }
    else {
        mapPin.annotation = annotation;

    }
    
    return mapPin;
}


#pragma mark -  Composer delegates


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Utility methods


// Add an annotation for an address object
- (void)addAnnotationForAddress:(Address *)address
{
    // Build address
    NSString *addressString = [NSString stringWithFormat:@"%@ %@, %@, %@", address.streetNumber, address.street, address.city, address.country];
    CLGeocoder *geocoder    = [[CLGeocoder alloc] init];
    // Get coordinate of the address
    [geocoder geocodeAddressString:addressString completionHandler:^(NSArray* placemarks, NSError* error){
        // For placemark found create a map annotation
        for (CLPlacemark* aPlacemark in placemarks)
        {
            // Process the placemark.
            TKPointAddressAnnotation *point = [[TKPointAddressAnnotation alloc] init];
            point.coordinate                = aPlacemark.location.coordinate;
            // Store address in annotation for futher manipulation (like sharing)
            // Setting the address will automatically set up the PointAnnotation
            point.annotationAddress         = address;
            [self.mapView addAnnotation:point];
        }
        
        // Warn user if invalid address. Only if the view has been pushed after cell selection. Otherwise the map tab could be to spammy.
        if (self.addresses.count == 1 && error) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.address.title", nil)
                                                            message:NSLocalizedString(@"alert.address.message", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"alert.dismiss", nil)
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (NSString *)addressBodyMessageToShare
{
    TKPointAddressAnnotation *selectedAnnotation = (TKPointAddressAnnotation *)self.mapView.selectedAnnotations.firstObject;
    Address *address = selectedAnnotation.annotationAddress;
    NSString *body = [NSString stringWithFormat:@"Address:\n%@ %@\n%@, %@, %@\n%@\n\nDescription:\n%@",
                      address.streetNumber, address.street,
                      address.zipcode, address.province, address.city,
                      address.country,
                      address.addressDescription];
    
    return body;
}

- (void)setupShareview
{
    TKShareView *shareView = [[TKShareView alloc] initWithFrame:CGRectMake(0,
                                                                           CGRectGetHeight(self.view.bounds) - (CGRectGetHeight(self.view.bounds) - 150) - 45,
                                                                           CGRectGetWidth(self.view.bounds),
                                                                           CGRectGetHeight(self.view.bounds) - 150)];
    shareView.frame = CGRectOffset(shareView.frame, 0, 1000);
    shareView.tag = 1;
    
    // Setup actions
    [shareView.shareEmailButton addTarget:nil action:@selector(shareEmailButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [shareView.shareMessageButton addTarget:nil action:@selector(shareMessageButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    // Add view
    [self.view addSubview:shareView];
    
    [UIView animateWithDuration:0.3 animations:^{
        shareView.frame = CGRectOffset(shareView.frame, 0, -1000);
    }];
}
@end
