//
//  TKEntityManager.m
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 21/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

#import "TKEntityManager.h"
#import "TKNSManagedObjectContext.h"
#import "Address.h"

@interface TKEntityManager()


// Core data context
@property (nonatomic) TKNSManagedObjectContext *context;


@end

@implementation TKEntityManager



/**
 *  Fetch all objects from core data for a specified entity
 *
 *  @param entityName Entity name
 *
 *  @return Array of objects
 */
+ (NSArray *)fetchAllForEntity:(NSString *)entityName
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity  = [NSEntityDescription entityForName:entityName inManagedObjectContext:[TKNSManagedObjectContext sharedInstance]];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *results =  [[NSArray alloc] initWithArray:[[TKNSManagedObjectContext sharedInstance] executeFetchRequest:fetchRequest error:&error]];
    
    return results;
}


/**
 *  Create and add an object for a specified entity. Uses KVC. Make sure dictionary key matches entity attributes
 *
 *  @param objectData NSDictionary containing data of new object. Must comply to KVC
 *  @param entityName Entity name
 */
+ (void)addObject:(NSDictionary *)objectData forEntity:(NSString *)entityName
{
    id newObject = [NSEntityDescription insertNewObjectForEntityForName:entityName
                                                 inManagedObjectContext:[TKNSManagedObjectContext sharedInstance]];
    
    // Use KVC to create object from NSDictionary
    [objectData enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [newObject setValue:obj forKey:key];
    }];
    
    [self save];
}


/**
 *  Remove object of core data
 *
 *  @param object Object to remove
 */
+ (void)removeObject:(id)object
{
    [[TKNSManagedObjectContext sharedInstance] deleteObject:object];
    [self save];
}


/**
 *  Save context
 */
+ (void)save
{
    NSError *error;
    if (![[TKNSManagedObjectContext sharedInstance] save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
}


#pragma mark - Getters

// Lazy get of context
- (TKNSManagedObjectContext *)context
{
    if (!_context) {
        _context = [TKNSManagedObjectContext sharedInstance];
    }
    
    return _context;
}

@end
