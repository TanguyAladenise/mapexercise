//
//  TKNSManagedObject.m
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 21/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

#import "TKNSManagedObjectContext.h"
#include "TKAppDelegate.h"

/**
 *  The core data managed object will be a singleton
 */
@implementation TKNSManagedObjectContext

//
+ (TKNSManagedObjectContext *)sharedInstance
{
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // The Core data template by Apple init the NSManagedObject for us in the app delegate
        TKAppDelegate *appDelegate = (TKAppDelegate *)[[UIApplication sharedApplication] delegate];
        // Retrieve the instance instanciated in the app delegate
        sharedInstance = appDelegate.managedObjectContext;
    });
    
    return sharedInstance;
}


@end
