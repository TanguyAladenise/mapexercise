//
//  TKShareView.h
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 22/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TKShareView : UIView

@property UIButton *shareEmailButton;
@property UIButton *shareMessageButton;
@property UIButton *closeButton;

@end
