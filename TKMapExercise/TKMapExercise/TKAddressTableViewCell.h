//
//  TKAddressTableViewCell.h
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 21/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TKAddressTableViewCell : UITableViewCell

@property IBOutlet UILabel *addressLabel;
@property IBOutlet UILabel *addressLabel2;

@end
