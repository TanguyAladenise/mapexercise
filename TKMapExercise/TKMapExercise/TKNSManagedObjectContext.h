//
//  TKNSManagedObject.h
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 21/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface TKNSManagedObjectContext : NSManagedObjectContext

/**
 *  Singleton access to shared instance
 *
 *  @return An instance of TKNSManagedObject 
 */
+ (TKNSManagedObjectContext *)sharedInstance;

@end
