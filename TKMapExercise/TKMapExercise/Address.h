//
//  Address.h
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 21/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Address : NSManagedObject

@property (nonatomic, retain) NSNumber * streetNumber;
@property (nonatomic, retain) NSString * street;
@property (nonatomic, retain) NSString * zipcode;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * province;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * addressDescription;

@end
