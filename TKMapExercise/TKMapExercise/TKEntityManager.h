//
//  TKEntityManager.h
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 21/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKEntityManager : NSObject


/**
 *  Fetch all objects from core data for a specified entity
 *
 *  @param entityName Entity name
 *
 *  @return Array of objects
 */
+ (NSArray *)fetchAllForEntity:(NSString *)entityName;


/**
 *  Create and add an object for a specified entity. Uses KVC. Make sure dictionary key matches entity attributes
 *
 *  @param objectData NSDictionary containing data of new object. Must comply to KVC
 *  @param entityName Entity name
 */
+ (void)addObject:(NSDictionary *)objectData forEntity:(NSString *)entityName;


/**
 *  Remove object of core data
 *
 *  @param object Object to remove
 */
+ (void)removeObject:(id)object;

@end
