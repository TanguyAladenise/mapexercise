//
//  Address.m
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 21/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

#import "Address.h"


@implementation Address

@dynamic streetNumber;
@dynamic street;
@dynamic zipcode;
@dynamic city;
@dynamic province;
@dynamic country;
@dynamic addressDescription;

@end
