//
//  TKNewAddressViewController.m
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 21/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

#import "TKNewAddressViewController.h"
#include "TKEntityManager.h"

@interface TKNewAddressViewController ()

// UI
@property IBOutlet UIButton *saveButton;
@property IBOutlet UIScrollView *scrollView;

// Use collection view for fast batch manipulation
@property IBOutletCollection(UITextField) NSArray *textFieldCollection;

// Access each field for form validation
@property IBOutlet UITextField *streetNumberField;
@property IBOutlet UITextField *streetField;
@property IBOutlet UITextField *zipcodeField;
@property IBOutlet UITextField *cityField;
@property IBOutlet UITextField *provinceField;
@property IBOutlet UITextField *countryField;
@property IBOutlet UITextField *descriptionField;


@end

@implementation TKNewAddressViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    // Quick fix to overcome the fact that UIScrollView does not pass the touchesBegan (To handle closing keyboard)
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyboard:)];
    [self.scrollView addGestureRecognizer:tapGesture];
    
    // Get notified of keyboard to handle view size
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    self.scrollView.contentSize = self.view.bounds.size;
}

#pragma mark - UI Action methods

// Save new address with core data
- (IBAction)saveAddressButtonPressed
{
    // Check input form
    if ([self checkForm]) {
        // Save address typed
        
        // Build data from inputs
        NSDictionary *data = @{ @"streetNumber"         : @([self.streetNumberField.text integerValue]),
                                @"street"               : self.streetField.text,
                                @"zipcode"              : self.zipcodeField.text,
                                @"city"                 : self.cityField.text,
                                @"province"             : self.provinceField.text,
                                @"country"              : self.countryField.text,
                                @"addressDescription"   : self.descriptionField.text
                              };
        
        [TKEntityManager addObject:data forEntity:@"Address"];
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.form.title", nil)
                                                        message:NSLocalizedString(@"alert.form.message", nil)
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"alert.dismiss", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - Utility methods


// Check if the form contains all data required
- (BOOL)checkForm
{
    BOOL isValid = YES;
    for (UITextField *textField in self.textFieldCollection) {
        
        NSLog(@"Testing a textField input");
        
        // Province is not required
        if (textField == self.provinceField) {
            break;
        }
        
        if (textField.text.length == 0) {
            NSLog(@"textField invalid");
            isValid = NO;
        }
    }
    
    return isValid;
}


// Close the keyboard when user touch away of textField
- (void)closeKeyboard:(UIGestureRecognizer *)gesture
{
    for (UITextField *textField in self.textFieldCollection) {
        [textField resignFirstResponder];
    }
}


#pragma mark - Notification


// List for keyboard activity to avoid field to be unreachable by user
- (void)keyboardDidShow:(NSNotification*)notification
{
    // Get keyboard size to adapt view
    NSDictionary *keyboardInfo    = [notification userInfo];
    NSValue *keyboardFrameBegin   = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, CGRectGetHeight(self.scrollView.frame) + CGRectGetHeight(keyboardFrameBeginRect));
}

- (void)keyboardDidHide:(NSNotification*)notification
{
    // Reinit position and contentSize
    self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, CGRectGetHeight(self.scrollView.frame));
    [UIView animateWithDuration:0.2 animations:^{
        self.scrollView.contentOffset = CGPointMake(0, 0);
    }];
}

@end
