//
//  TKShareView.m
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 22/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

// Two share options + closing button
static const    int numberOfButtons = 3;


#import "TKShareView.h"

@implementation TKShareView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Setup view
        self.backgroundColor = [UIColor lightGrayColor];
        
        // Email
        self.shareEmailButton                      = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.shareEmailButton setTitle:NSLocalizedString(@"button.shareMail", nil) forState:UIControlStateNormal];
        self.shareEmailButton.frame                = CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame) / numberOfButtons);
        self.shareEmailButton.backgroundColor      = [UIColor colorWithRed:0.309 green:0.847 blue:1.000 alpha:1.000];
        self.shareEmailButton.titleLabel.textColor = [UIColor whiteColor];
        [self addSubview:self.shareEmailButton];
        
        // Message
        self.shareMessageButton                      = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.shareMessageButton setTitle:NSLocalizedString(@"button.shareMessage", nil) forState:UIControlStateNormal];
        self.shareMessageButton.frame                = CGRectMake(0, CGRectGetMaxY(self.shareEmailButton.frame), CGRectGetWidth(frame), CGRectGetHeight(frame) / numberOfButtons);
        self.shareMessageButton.backgroundColor      = [UIColor colorWithRed:0.255 green:1.000 blue:0.430 alpha:1.000];
        self.shareMessageButton.titleLabel.textColor = [UIColor whiteColor];
        [self addSubview:self.shareMessageButton];
        
        self.closeButton                      = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeButton setTitle:NSLocalizedString(@"button.close", nil) forState:UIControlStateNormal];
        self.closeButton.frame                = CGRectMake(0, CGRectGetMaxY(self.shareMessageButton.frame), CGRectGetWidth(frame), CGRectGetHeight(frame) / numberOfButtons);
        self.closeButton.backgroundColor      = [UIColor darkGrayColor];
        self.closeButton.titleLabel.textColor = [UIColor whiteColor];
        [self addSubview:self.closeButton];
        
        // Close action
        [self.closeButton addTarget:nil action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
        
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return self;
}


// Manual rotation
- (void)layoutSubviews
{
    CGRect frame                  = self.bounds;
    self.shareEmailButton.frame   = CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame) / numberOfButtons);
    self.shareMessageButton.frame = CGRectMake(0, CGRectGetMaxY(self.shareEmailButton.frame), CGRectGetWidth(frame), CGRectGetHeight(frame) / numberOfButtons);
    self.closeButton.frame        = CGRectMake(0, CGRectGetMaxY(self.shareMessageButton.frame), CGRectGetWidth(frame), CGRectGetHeight(frame) / numberOfButtons);
}


// Close view animated
- (void)closeView
{
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectOffset(self.frame, 0, 1000);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
