//
//  main.m
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 21/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TKAppDelegate class]));
    }
}
