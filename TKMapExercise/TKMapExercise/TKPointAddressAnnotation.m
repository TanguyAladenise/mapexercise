//
//  TKPointAddressAnnotation.m
//  TKMapExercise
//
//  Created by Tanguy Aladenise on 22/06/14.
//  Copyright (c) 2014 aladenise. All rights reserved.
//

#import "TKPointAddressAnnotation.h"
#import "Address.h"

@implementation TKPointAddressAnnotation

- (void)setAnnotationAddress:(Address *)annotationAddress
{
    _annotationAddress = annotationAddress;
    
    self.title    = [NSString stringWithFormat:@"%@ %@", _annotationAddress.streetNumber, _annotationAddress.street];
    self.subtitle = _annotationAddress.addressDescription;
}

@end
